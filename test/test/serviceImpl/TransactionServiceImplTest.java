/**
 * Archie
 */
package test.serviceImpl;

import static org.junit.Assert.fail;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;

import model.Account;
import service.TransactionService;
import serviceImpl.TransactionServiceImpl;

/**
 * @author Archie
 *
 */
public class TransactionServiceImplTest {
	private static final double AMOUNT_TRANSFER_A = 10;
	private static final double AMOUNT_TRANSFER_B = 11;
	private static final double AMOUNT_DEFAULT = 10;
	private static final String USERA = "archie1";
	private static final String USERB = "archie2";
	
	private  TransactionService transService ;
	
	@BeforeEach
	public void setup() {
		transService  = new TransactionServiceImpl();
	}
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public  void setUpBeforeClass() throws Exception {
		transService.insertAccount(1l, USERA, AMOUNT_DEFAULT);
		transService.insertAccount(2l, USERB, AMOUNT_DEFAULT);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public void tearDownAfterClass() throws Exception {
		List<Account> lstResult = transService.getLstAccount();
		System.out.println(lstResult.toString());
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link serviceImpl.TransactionServiceImpl#transfersMoney(model.Account, model.Account, java.lang.Double)}.
	 */
	@Test
	public final void testTransfersMoney() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link serviceImpl.TransactionServiceImpl#transfersMoneyWithLock(model.Account, model.Account, java.lang.Double)}.
	 */
	@Test
	public final void testTransfersMoneyWithLock() {
		fail("Not yet implemented"); // TODO
	}

}

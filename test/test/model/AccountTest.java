/**
 * Archie
 */
package test.model;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Archie
 *
 */
public class AccountTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link model.Account#Account()}.
	 */
	@Test
	public void testAccount() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link model.Account#Account(java.sql.ResultSet)}.
	 */
	@Test
	public void testAccountResultSet() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link model.Account#checkAccountBalance(double)}.
	 */
	@Test
	public void testCheckAccountBalance() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link model.Account#Account(java.lang.Long, java.lang.String, java.lang.Double)}.
	 */
	@Test
	public void testAccountLongStringDouble() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link model.Account#getId()}.
	 */
	@Test
	public void testGetId() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link model.Account#setId(java.lang.Long)}.
	 */
	@Test
	public void testSetId() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link model.Account#getUserName()}.
	 */
	@Test
	public void testGetUserName() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link model.Account#setUserName(java.lang.String)}.
	 */
	@Test
	public void testSetUserName() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link model.Account#getBalance()}.
	 */
	@Test
	public void testGetBalance() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link model.Account#setBalance(java.lang.Double)}.
	 */
	@Test
	public void testSetBalance() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link model.Account#toString()}.
	 */
	@Test
	public void testToString() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link java.lang.Object#Object()}.
	 */
	@Test
	public void testObject() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link java.lang.Object#getClass()}.
	 */
	@Test
	public void testGetClass() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link java.lang.Object#hashCode()}.
	 */
	@Test
	public void testHashCode() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link java.lang.Object#equals(java.lang.Object)}.
	 */
	@Test
	public void testEquals() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link java.lang.Object#clone()}.
	 */
	@Test
	public void testClone() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link java.lang.Object#toString()}.
	 */
	@Test
	public void testToString1() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link java.lang.Object#notify()}.
	 */
	@Test
	public void testNotify() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link java.lang.Object#notifyAll()}.
	 */
	@Test
	public void testNotifyAll() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link java.lang.Object#wait(long)}.
	 */
	@Test
	public void testWaitLong() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link java.lang.Object#wait(long, int)}.
	 */
	@Test
	public void testWaitLongInt() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link java.lang.Object#wait()}.
	 */
	@Test
	public void testWait() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link java.lang.Object#finalize()}.
	 */
	@Test
	public void testFinalize() {
		fail("Not yet implemented");
	}

}

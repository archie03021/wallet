/**
 * Archie
 */
package connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author Archie
 *
 */
public class MysqlDb {
	private Connection connect = null;
	private Statement statement = null;
	private PreparedStatement preparedStatement = null;
	private ResultSet resultSet = null;

	private static final String CONNECTION_URL = "jdbc:mysql://localhost/demo?useSSL=false";
	private static final String USERNAME = "root";
	private static final String PASSWORD = "root";

	public MysqlDb() {
		try {
			// Context initCtx = new InitialContext();
			// Context envCtx = (Context) initCtx.lookup("java:comp/env");
			if(statement== null || connect == null) {
				Class.forName("com.mysql.jdbc.Driver");  
				connect = DriverManager.getConnection(CONNECTION_URL, USERNAME, PASSWORD);
				statement = connect.createStatement();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ResultSet getResultSet() {
		return resultSet;
	}

	public boolean setQuerry(String sql) throws Exception {
		if (connect != null) {
			preparedStatement = connect.prepareCall(sql);
			return true;
		} else {
			preparedStatement = connect.prepareCall(sql);
			return true;
		}
	}

	public void close() {
		try {
			if (resultSet != null) {
				resultSet.close();
			}
			if (statement != null) {
				statement.close();
			}
			if (connect != null) {
				connect.close();
			}
		} catch (Exception e) {

		}
	}

	public ResultSet executeQuery() throws SQLException {
		if (preparedStatement == null)
			return null;
		resultSet = preparedStatement.executeQuery();
		return resultSet;
	}

	public Integer executeUpdate() throws SQLException {
		if (preparedStatement == null)
			return 0;
		return preparedStatement.executeUpdate();
	}

	public boolean execute() throws SQLException {
		if (preparedStatement == null)
			return false;
		return preparedStatement.execute();
	}

	public void rollback() {
		if (this.connect != null) {
			try {
				this.connect.rollback();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public void setAutoCommit(boolean value) {
		if (this.connect != null)
			try {
				this.connect.setAutoCommit(value);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}

	public void commit() throws SQLException {
		this.connect.commit();
	}

	public void setString(int index, String value) throws SQLException {
		preparedStatement.setString(index, value);
	}

	public void setLong(int index, Long value) throws SQLException {
		preparedStatement.setLong(index, value);
	}

	public void setDouble(int index, Double value) throws SQLException {
		preparedStatement.setDouble(index, value);
	}

}
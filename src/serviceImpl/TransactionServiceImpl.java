/**
 * Archie
 */
package serviceImpl;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import connection.MysqlDb;
import model.Account;
import service.TransactionService;

/**
 * @author Archie
 *
 */
public class TransactionServiceImpl implements TransactionService{
	
	
	@Override
	public Account getAccount(Long id) {
		long t1 = System.currentTimeMillis();
		Account account = null;
		String strQuerry = "select * from Account where id = ?";
		MysqlDb db = new MysqlDb();
		try {
			db.setQuerry(strQuerry);
			db.setLong(1, id);
			ResultSet result = db.executeQuery();

			if (result.next())
				account = new Account(result);

			long t2 = System.currentTimeMillis();
			System.out.println("TIME getAccount " + (t2-t1));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} finally {
			if (db != null) {
				db.close();
			}
		}
		return account;
	}

	@Override
	public List<Account> getLstAccount() {
		long t1 = System.currentTimeMillis();
		List<Account> lstAccount = new ArrayList<Account>();
		Account account = null;
		String strQuerry = "select * from Account ";
		MysqlDb db = new MysqlDb();
		try {
			db.setQuerry(strQuerry);
			ResultSet result = db.executeQuery();

			while (result.next()) {
				System.out.println(result.getInt(1) + "  " + result.getString(2) + "  " + result.getString(3));
				account = new Account(result);
				lstAccount.add(account);
			}

			long t2 = System.currentTimeMillis();
			System.out.println("TIME getLstAccount " + (t2 - t1));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			if (db != null) {
				db.close();
			}
		}
		return lstAccount;
	}

	@Override
	public Account insertAccount(Long id, String userName, Double balance) {
		long t1 = System.currentTimeMillis();
		Account account = null;
		String strQuerry = "insert into Account (id , userName , balance ) values (?,?,?) ";
		MysqlDb db = new MysqlDb();
		try {
			db.setQuerry(strQuerry);
			db.setLong(1, id);
			db.setString(2, userName);
			db.setDouble(3, balance);
			ResultSet result = db.executeQuery();

			if (result.next())
				account = new Account(result);

			//System.out.println(result.getInt(1) + "  " + result.getString(2) + "  " + result.getString(3));
			long t2 = System.currentTimeMillis();
			System.out.println("TIME insertAccount " + (t2 - t1));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (db != null) {
				db.close();
			}
		}
		return account;
	}

	@Override
	public boolean updateBalance(Long id, Double amount) {
		String strQuerry = "update Account set balance = balance + ? where id =?";
		MysqlDb db = new MysqlDb();
		try {
			db.setQuerry(strQuerry);
			db.setDouble(1, amount);
			db.setLong(2, id);

			db.setAutoCommit(false);
			Integer result = db.executeUpdate();
			if (1 == result) {
				db.commit();
				return true;
			} else {
				db.rollback();
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			
			if (db != null) {
				db.rollback();
				db.setAutoCommit(true);
			}
			return false;
		} finally {
			if (db != null) {
				db.setAutoCommit(true);
				db.close();
			}
		}
	}


	@Override
	public boolean transfersMoney(Account accountA, Account accountB, Double amount) {
		// check exist Account
		if (accountA == null || accountB ==null) {
			System.out.println("account is null");
			return false;
		}
		if (accountA.getId()  == accountB.getId()) {
			System.out.println("it's same account");
			return false;
		} 
		if (amount <= 0)  {
			System.out.println("amount transfer <0");
			return false;
		}
	
		synchronized(accountA) {
			System.out.println(":::::::::::::::::::::::::::::" + Thread.currentThread().getName());
			// check balance >0
			Double balanceA = accountA.getBalance();
			if (balanceA == null || balanceA <= 0) {
				System.out.println("Balance is not enough , user : " +accountA.getUserName());
				return false;
			}
				
			// check balance Account
			if (balanceA >= amount) {
				// transfer
				updateBalance(accountA.getId(), amount * (-1));
			} else {
				return false;
			}
			try {
				//test
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// Release the lock of A ,Acquire the lock of B
		synchronized (accountB) {
			updateBalance(accountB.getId(), amount);
		}
		
		return false;
	}


	@Override
	public boolean updateDefault(Long id, Double amount) {
		String strQuerry = "update Account set balance =? where id =?";
		MysqlDb db = new MysqlDb();
		try {
			db.setQuerry(strQuerry);
			db.setDouble(1, amount);
			db.setLong(2, id);

			db.setAutoCommit(false);
			Integer result = db.executeUpdate();
			if (1 == result) {
				db.commit();
				return true;
			} else {
				db.rollback();
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			
			if (db != null) {
				db.rollback();
				db.setAutoCommit(true);
			}
			return false;
		} finally {
			if (db != null) {
				db.setAutoCommit(true);
				db.close();
			}
		}
	}

	@Override
	public boolean transfersMoneyWithLock(Account accountA, Account accountB, Double amount) {
		// check exist Account
		if (accountA == null || accountB == null) {
			System.out.println("account is null");
			return false;
		}
		if (accountA.getId() == accountB.getId()) {
			System.out.println("it's same account");
			return false;
		}
		if (amount <= 0) {
			System.out.println("amount transfer <0");
			return false;
		}
		
		try {
			accountA.getLock().lock();
			System.out.println(":::::::::::::::::::::::::::::" + Thread.currentThread().getName());
			// check balance >0
			Double balanceA = accountA.getBalance();
			if (balanceA == null || balanceA <= 0) {
				System.out.println("Balance is not enough , user : " + accountA.getUserName());
				return false;
			}

			// check balance Account
			if (balanceA >= amount) {
				// transfer
				updateBalance(accountA.getId(), amount * (-1));
			} else {
				return false;
			}
			try {
				//FIXME for test
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} finally {
			accountA.getLock().unlock();
		}
		
		// Release the lock of A ,Acquire the lock of B
		try {
			accountB.getLock().lock();
			updateBalance(accountB.getId(), amount);
		} finally {
			accountB.getLock().unlock();
		}
		return false;
	}
}

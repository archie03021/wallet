/**
 * Archie
 */
package model;

import java.sql.ResultSet;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import service.TransactionService;
import serviceImpl.TransactionServiceImpl;

/**
 * @author Archie
 *
 */
public class Account {
	public static final double MIN_BALANCE = 0;

	private Long id;
	private String userName;
	private Double balance;
	private Lock lock = new ReentrantLock();
	
	public Lock getLock() {
		return lock;
	}

	public void setLock(Lock lock) {
		this.lock = lock;
	}

	public Account() {
	}

	public Account(ResultSet resultSet) {
		try {
			this.id = resultSet.getLong(1);
			this.userName = resultSet.getString(2);
			this.balance = resultSet.getDouble(3);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
//TODO test
//FIXME yesterday
//	public void withdraw(Long id, double withdrawAmount) {
//		System.out.println(id + " check: " + withdrawAmount);
//		if (withdrawAmount > MIN_BALANCE) {
//			synchronized (this) {
//				if (checkAccountBalance(withdrawAmount)) {
//					// connect Database
//
//					balance -= withdrawAmount;
//					System.out.println(id + " withdraw successful: " + withdrawAmount);
//				} else {
//					System.out.println(id + " withdraw error!");
//				}
//			}
//
//		}
//		System.out.println(id + " see balance: " + balance);
//	}
//
//	public synchronized void withdrawWhenBalanceEnough(String threadName, double withdrawAmount) {
//
//		System.out.println(threadName + " check: " + withdrawAmount);
//
//		while (!checkAccountBalance(withdrawAmount)) {
//			System.out.println(threadName + " wait for balance enough");
//			try {
//				wait();
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//
//		try {
//			Thread.sleep(2000);
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		}
//
//		balance -= withdrawAmount;
//		System.out.println(threadName + " withdraw successful: " + withdrawAmount);
//	}
//
//	public synchronized void deposit(String threadName, double depositAmount) {
//		System.out.println(threadName + " deposit: " + depositAmount);
//		try {
//			// connect database
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//		balance += depositAmount;
//
//		notify();
//	}

	public Account(Long id, String userName, Double balance) {
		super();
		this.id = id;
		this.userName = userName;
		this.balance = balance;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Double getBalance() {
		return balance;
	}

	public void setBalance(Double balance) {
		this.balance = balance;
	}

	@Override
	public String toString() {
		return "Account [id=" + id + ", userName=" + userName + ", balance=" + balance + "]";
	}

}

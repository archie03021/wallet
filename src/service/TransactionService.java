/**
 * Archie
 */
package service;

import java.util.List;

import model.Account;

/**
 * @author Archie
 *
 */
public interface TransactionService {
	public  Account getAccount(Long id);

	public List<Account> getLstAccount();

	public Account insertAccount(Long id, String userName, Double balance);

	public boolean updateBalance(Long id, Double amount);

	public boolean transfersMoney(Account accountA, Account accountB, Double amount);
	
	public boolean transfersMoneyWithLock(Account accountA, Account accountB, Double amount);
	
	public boolean updateDefault(Long id, Double amount);
}

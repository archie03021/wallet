/**
 * Archie
 */

import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import model.Account;
import service.TransactionService;
import serviceImpl.TransactionServiceImpl;

/**
 * @author Archie
 *
 */
public class Main {

	private static final double AMOUNT_TRANSFER_A = 10;
	private static final double AMOUNT_TRANSFER_B = 11;
	private static final double AMOUNT_DEFAULT = 10;

	public static void main(String[] args) {
		TransactionService transService = new TransactionServiceImpl();
		//Update account default balance
//		transService.updateDefault(1l, 9d);
//		transService.updateDefault(2l, 11d);
		//get account
		Account accA = transService.getAccount(1l);
		Account accB = transService.getAccount(2l);
		new Thread() {
			
			@Override
			public void run() {
				transService.transfersMoney(accA, accB, AMOUNT_TRANSFER_A);
			}
		}.start();

		new Thread() {
			
			@Override
			public void run() {
				transService.transfersMoney(accB, accA, AMOUNT_TRANSFER_B);
			}
		}.start();
		
		//List<Account> lstResult = transService.getLstAccount();
		//System.out.println(lstResult.toString());
		//test
		//transService.updateDefault(1l, 10);transService.updateDefault(2l, 10);
		

	}
}

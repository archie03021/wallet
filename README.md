# Wallet

Step 1 create Database mysql :
database name : demo
username/password :demo/demo
Setup connection config file at class MysqlDb
	private static final String CONNECTION_URL = "jdbc:mysql://localhost/demo?useSSL=false";
	private static final String USERNAME = "root";
	private static final String PASSWORD = "root";
	
Step 2
I provide 2 ways to synchonize money transfer  :
    1. using synchonized key word in the method : transfersMoney
    2. using class java.util.concurrent.locks.Lock :  method is transfersMoneyWithLock
    You can change it on Main function to test
